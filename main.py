from flask import Flask, send_file


app = Flask(__name__)


@app.route("/", methods=["GET"])
def root():
    return "Hello World!"


@app.route("/leave", methods=["GET"])
def leave():
    return "Boodbye, Moonman."


@app.route("/test", methods=["GET"])
def test():
    return send_file("./test.txt")


@app.route("/home", methods=["GET"])
def home():
    return "Welcome back, Mr. Stark"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6969)
